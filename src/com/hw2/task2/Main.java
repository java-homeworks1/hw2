package com.hw2.task2;

import java.util.*;

public class Main {

    public static boolean checkNumber(List<Integer> arr, int div) {
        boolean result = true;
        Integer avgValue = (Collections.max(arr) + Collections.min(arr)) / 2;
        for (int number : arr) {
            if (div != 0 && number % div != 0) {
                result = false;
                break;
            } else if (number < avgValue - div) {
                result = false;
                break;
            } else if (number > avgValue + div) {
                result = false;
                break;
            } else {
                result = true;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.print("Input number sequence, separated by space: ");
        Scanner scanner = new Scanner(System.in);

        String line = scanner.nextLine();
        String[] lineNumbers = line.trim().replaceAll(" +", " ").split(" ");

        List<Integer> numbers = new ArrayList<>();
        for (int i = 0; i < lineNumbers.length; i++) {
            numbers.add(Integer.parseInt(lineNumbers[i]));
        }

        ArrayList<Integer> digits = new ArrayList<>();
        for (int e : numbers) {
            if (checkNumber(numbers, e)) {
                digits.add(e);
            }
        }
        int result = digits.size() > 0 ? digits.get(digits.indexOf(Collections.min(digits))) : -1;
        System.out.println(result);
    }
}
