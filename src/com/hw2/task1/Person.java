package com.hw2.task1;

public class Person {
    private String name;
    private Integer score;
    private Integer scoreCache;

    public Person(String name, Integer score) {
        this.name = name;
        this.score = score;
        this.scoreCache = score;
    }

    public Integer getScoreCache() {
        return scoreCache;
    }

    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer newScore) {
        this.score = this.score + newScore;
        this.scoreCache = this.score;
    }

    public void minusScore(Integer newScore) {
        this.scoreCache = this.scoreCache - newScore;
    }
}
