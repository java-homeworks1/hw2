package com.hw2.task1;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        List<Person> personQueue = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter number of players score records: ");
        Integer numberOfPlayers = Integer.parseInt(scanner.nextLine());

        String[] arr = new String[numberOfPlayers];
        Integer correctValues = 0;
        while (correctValues != arr.length) {
            System.out.print("Enter new player & score: ");
            String userInput = scanner.nextLine();
            if (Pattern.matches("\\w+\\s\\d+", userInput)) {
                arr[correctValues] = userInput;
                correctValues++;
            } else {
                System.out.println("Wrong input format");
            }
        }

        for (String i : arr) {
            String[] data = i.split(" ");
            String personName = data[0];
            Integer personScore = Integer.parseInt(data[1]);
            Person p = personList.stream()
                    .filter(person -> personName.equals(person.getName())).findFirst().orElse(null);

            if (p != null) {
                personList.get(personList.indexOf(p)).setScore(personScore);
            } else {
                personList.add(new Person(personName, personScore));
            }
            personQueue.add(new Person(personName, personScore));
        }
        Person maxScorePerson = Collections.max(personList, Comparator.comparing(Person::getScore));
        Integer maxScore = maxScorePerson.getScore();
        Integer maxScoresCount = personList.stream()
                .filter(person -> person.getScore() == maxScore)
                .collect(Collectors.toList()).size();

        if (maxScoresCount == 1) {
            System.out.println("Winner person is " + maxScorePerson.getName() + " with score = " + maxScorePerson.getScore());
        } else {
            for (Person p : personQueue) {
                Person personIndex = personList.stream()
                        .filter(person -> p.getName().equals(person.getName())).findFirst().orElse(null);
                Person currentPerson = personList.get(personList.indexOf(personIndex));
                currentPerson.minusScore(p.getScore());

                if (currentPerson.getScoreCache() == 0 && currentPerson.getScore() == maxScore) {
                    System.out.println("Winner person is " + currentPerson.getName());
                    break;
                }
            }
        }
    }
}
